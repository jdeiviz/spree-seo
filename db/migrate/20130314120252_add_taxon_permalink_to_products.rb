class AddTaxonPermalinkToProducts < ActiveRecord::Migration
  def self.up
    add_column :spree_products, :taxon_permalink_id, :integer
    add_index :spree_products, :taxon_permalink_id
  end

  def self.down
    remove_column :spree_products, :taxon_permalink_id
    remove_index :spree_products, :taxon_permalink_id
  end
end
