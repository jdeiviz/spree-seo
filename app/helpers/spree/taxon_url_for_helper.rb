module Spree
  module TaxonUrlForHelper    
    def self.included(base)
      base.class_eval do        
        def url_for(options = {})
          original = super(options)
          return original unless original.starts_with?('/t/')
          original.gsub('/t/', '/')
        end
      end
    end
  end
end