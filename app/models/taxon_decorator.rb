Spree::Taxon.class_eval do
  def set_permalink        
    self.permalink = name.to_url if permalink.blank?
  end
end