module SpreeSeo
  class Engine < Rails::Engine
    require 'spree/core'
    isolate_namespace Spree
    engine_name 'spree_seo'    

    config.autoload_paths += %W(#{config.root}/lib)

    initializer "spree_seo.add_middleware" do |app|
      app.middleware.use SpreeSeo::Middleware::SeoAssist
    end
    
    # use rspec for tests
    config.generators do |g|
      g.test_framework :rspec
    end

    def self.activate
      Dir.glob(File.join(File.dirname(__FILE__), '../../app/**/*_decorator*.rb')) do |c|
        Rails.configuration.cache_classes ? require(c) : load(c)
      end

      ApplicationController.helper Spree::TaxonUrlForHelper
      ApplicationController.helper Spree::ProductPathHelper
    end

    config.to_prepare &method(:activate).to_proc
  end
end
