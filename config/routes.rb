Spree::Core::Engine.routes.draw do
  # Add your extension routes here

  resources :products, :path => :productos, :except => :show

  #match '/t/*id' => proc { [404, {}, ['']] }
  match '/t/*id' => redirect { |params, req| "/#{URI.escape(params[:id])}" + (params[:page].nil? ? "" : "?page=#{params[:page]}") }
end

Spree::Core::Engine.routes.append do
  # Add your extension routes here  

  match '/:taxon/:id', :to => 'products#show', :as => :product#, :constraints => { :id => /\S+\/\S+/ }

  # route globbing for pretty nested taxon and product paths
  match '/:id', :to => 'taxons#show', :as => :nested_taxons  

end