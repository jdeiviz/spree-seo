Spree::Product.class_eval do
    belongs_to :taxon_permalink, :class_name => 'Spree::Taxon'

    attr_accessible :taxon_permalink_id

end