Spree::HomeController.class_eval do
 
  before_filter :set_canonical, :only => :index

  private

    def set_canonical
      @canonical_url = ("http://" + Spree::Config[:site_url] + root_path).html_safe
      current_url = "#{request.protocol}#{request.host_with_port}#{request.path}"
      redirect_to @canonical_url + (request.query_string ? "?#{request.query_string}" : ''), :status => :moved_permanently and return if @canonical_url != current_url
    end

end